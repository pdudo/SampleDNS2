package db

import (
	"log"
	"time"

	"gitee.com/pdudo/SampleDNS2/Conf"
	_ "gitee.com/pdudo/SampleDNS2/log"
	"gopkg.in/redis.v5"
)

var Rdb *redis.Client
var DnsConf Conf.ConfMain


func Set(key string, value interface{}, expiration time.Duration) (string, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB Set key: ", key, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.Set(key, value, expiration).Result()
}

func Get(key string) (string, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB Get key: ", key, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.Get(key).Result()
}

func Scan(cursor uint64, match string, count int64) ([]string, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB Scan Match: ", match, "Count: ", count, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()

	var keys []string
	var scanCurosr uint64
	var err error
	keys , scanCurosr , err = Rdb.Scan(cursor, match, count).Result()
	if err != nil {
		return keys,err
	}

	for scanCurosr > 0 {
		var OnceScanKeys []string
		OnceScanKeys , scanCurosr , err = Rdb.Scan(scanCurosr, match, count).Result()
		if err != nil {
			return keys,err
		}
		keys = append(keys, OnceScanKeys...)
	}
	return keys,nil
}

func Del(keys ...string) (int64, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB Del keys: ", keys, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.Del(keys...).Result()
}

func HExistsResult(key, filed string) (bool, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB HExistsResult keys: ", key, "filed: ", filed, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.HExists(key, filed).Result()
}

func HExistsVal(key, filed string) bool {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB HExistsVal keys: ", key, "filed: ", filed, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.HExists(key, filed).Val()
}

func HGet(key, filed string) (string, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB HGet keys: ", key, "filed: ", filed, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.HGet(key, filed).Result()
}

func HSet(key, filed string, value interface{}) (bool, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB HSet keys: ", key, "filed: ", filed, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.HSet(key, filed, value).Result()
}

func HSetVal(key, filed string, value interface{}) bool {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB HSetVal keys: ", key, "filed: ", filed, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.HSet(key, filed, value).Val()
}

func HDel(key string, fields ...string) (int64, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB HDel keys: ", key, "fileds: ", fields, " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.HDel(key, fields...).Result()
}

func HGetAll(key string) (map[string]string, error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB HGetAll keys: ", key, "fileds: ", " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.HGetAll(key).Result()
}

func Keys(pattern string)([]string , error) {
	startTime := time.Now().UnixNano()
	defer func() {
		log.Println("DB keys : ", pattern,  " Exec Time: ", (time.Now().UnixNano() - startTime))
	}()
	return Rdb.Keys(pattern).Result()
}
