package Conf

import (
	_ "gitee.com/pdudo/SampleDNS2/log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

type ConfMain struct {
	ConfGlobal Global `yaml:"Global" json:"Global"`
	ConfDNS DNS `yaml:"DNS" json:"DNS"`
	ConfWeb Web `yaml:"Web" json:"Web"`
}

type Global struct {
	RedisHost string `yaml:"RedisHost" json:"RedisHost"`
	Auth string `yaml:"Auth" json:"Auth"`
	DB int `yaml:"DB" json:"DB"`
}

type DNS struct {
	Bind string `yaml:"bind" json:"bind"`
	ProxyDNS string  `yaml:"ProxyDNS" json:"ProxyDNS"`
	ProxyDNSPort int `yaml:"ProxyDNSPort" json:"ProxyDNSPort"`
	OnlineStatus bool `yaml:"OnlineStatus" json:"OnlineStatus"`
}

type Web struct {
	Bind string `yaml:"bind" json:"bind"`
	RegisterKeys string `yaml:"RegisterKeys" json:"RegisterKeys"`
}

// 读取配置
func (serverConf *ConfMain) LoadConf(conf string)() {
	f , err := os.Open(conf)
	if err != nil {
		log.Panic("open files errors " , err)
	}
	defer f.Close()

	buf , err := ioutil.ReadAll(f)
	if err != nil {
		log.Panic("read byte error" , err)
	}


	err = yaml.Unmarshal(buf,&serverConf)
	if err != nil {
		log.Panic("load yaml conf error " , err)
	}

	log.Println("read yaml file successful , conf value" , serverConf)
}