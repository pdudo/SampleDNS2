package web

type showDNSInfo struct {
	ID int `json:"ID"`
	DomainName string `json:"DomainName"`
	ServerKeys string `json:"ServerKeys"`
	Type string `json:"Type"`
	Ip string `json:"Ip"`
	CreateTime string `json:"CreateTime"`
	UpdateTime string `json:"UpdateTime"`
	Remark string `json:"Remark"`
	Status string `json:"Status"`
}

type returnData struct {
	Code int `json:"code"`
	Data []showDNSInfo `json:"data"`
	Msg string`json:"msg"`
}

type DnsSaveInfo struct {
	ID int `json:"id"`
	DomainName string `json:"DomainName"`
	Type string `json:"Type"`
	Ip string `json:"Ip"`
	CreateTime string `json:"CreateTime"`
	UpdateTime string `json:"UpdateTime"`
	Remark string `json:"Remark"`
}

