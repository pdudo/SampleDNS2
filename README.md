# SampleDNS2

使用golang实现了DNS最简单的A/CNAME协议，协议详见: https://gitee.com/pdudo/SampleDNSTool

## 1. 快速体验

### 1.1 Docker/Kubernetes 体验

#### Docker 
> SampleDNS2 使用Redis作为数据库，请提前构建Redis数据库


```bash
# docker run -d --name dns -e redisHost="192.168.1.5:6379" -p 53:53/UDP -p 53:53/TCP -p 5001:5001/TCP docker.io/2859413527/sample-dns
```

Docker 环境变量
```bash
redisHost: redis数据库 默认值: 127.0.0.1:6379
redisAuth: redis秘密 默认值: ''
redisDB: redis 数据库 默认值: 0

dnsBind: dns监听套接字 默认值: 0.0.0.0:53
proxyDNSHost: dns代理服务器 默认值: 114.114.114.114
proxyDNSPort: dns代理服务器端口 默认值: 53
dnsOnlineStatus: dns在线请求累计值打印 默认值: false

webBind: dns管理页面监听套接字 默认值: 0.0.0.0:5001
RegisterKeys: Redis注册前缀  默认值: Xle
```

#### Kubernetes
> 192.168.1.5:6379 为本地使用的redis,请apply之前，修改为自己的redis数据库
```yaml
apiVersion: apps/v1
kind: Deployment

metadata:
  name: sampledns
  namespace: default

spec:
  replicas: 3

  selector:
    matchLabels:
      app: dns

  template:
    metadata:
      labels:
        app: dns

    spec:
      containers: 
        - name: sampledns2
          image: docker.io/2859413527/sample-dns

          env:
          - name: redisHost
            value: '192.168.1.5:6379'

          - name: proxyDNSHost
            value: '8.8.8.8'

          - name: dnsOnlineStatus
            value: 'false'
        
          ports:
          - name: tcpdns
            containerPort: 53
            protocol: TCP
        
          - name: udpdns
            containerPort: 53
            protocol: UDP

          - name: web
            containerPort: 5001
            protocol: TCP

---
apiVersion: v1
kind: Service

metadata:
  name: dnsservices
  namespace: default

spec:
  selector:
    app: dns

  type: NodePort

  ports:
  - name: tcpdns
    port: 53
    targetPort: 53
    protocol: TCP

  - name: udpdns
    port: 53
    targetPort: 53
    nodePort: 53
    protocol: UDP

    
  - name: web
    port: 5001
    targetPort: 5001
    nodePort: 5001
    protocol: TCP

```

**启动**
```bash
# kubectl apply -f sampledns.yaml
```

### 1.2 二进制包体验

> SampleDNS2 使用Redis作为数据库，请提前构建Redis数据库

#### 1.2.1 下载可执行文件

```bash
wget https://gitee.com/pdudo/SampleDNS2/attach_files/989795/download/SampleDNS2-v0.3.2-alpha-linux-amd64 -O SampleDNS2
chmod +x SampleDNS2
```

### 1.2.2 创建配置文件

支持 yaml 和 json 格式

yaml 配置文件
```yaml
# cat conf.yaml 
# Global
Global:
  RedisHost: "127.0.0.1:6379" # 存放记录的redis数据库
  Auth: "" # 密码
  DB: 0 # 库

# DNS
DNS:
  bind: "0.0.0.0:53" # DNS服务器监听TCP/UDP套接字
  ProxyDNS: "114.114.114.114" # 代理DNS服务器
  ProxyDNSPort: 53 # 代理服务器端口
  OnlineStatus: true

# Web
Web:
  bind: "0.0.0.0:5001" # Web http 监听地址
  RegisterKeys: "Xle123"
#
```

json 配置文件
```json
{
  "Global": {
    "RedisHost": "127.0.0.1:6379",
    "Auth": "",
    "DB": 0
  },
  "DNS": {
    "bind": "0.0.0.0:53",
    "ProxyDNS": "114.114.114.114",
    "ProxyDNSPort": 53,
    "OnlineStatus": true
  },
  "Web": {
    "bind": "0.0.0.0:5001",
    "RegisterKeys": "Xle123"
  }
}
```

### 1.3.3 执行程序

```bash
# ./SampleDNS2 -c conf.json
2022/03/08 21:54:36 conf.go:54: read yaml file successful , conf value &{{127.0.0.1:6379  0} {0.0.0.0:53 114.114.114.114 53 true} {0.0.0.0:5001 Xle123}}
2022/03/08 21:54:36 main.go:105: connect redis successful {127.0.0.1:6379  0}
2022/03/08 21:54:36 web.go:25: Web Server Start  0.0.0.0:5001
2022/03/08 21:54:36 main.go:142: DNS Server UDP Start successful  0.0.0.0:53
2022/03/08 21:54:36 main.go:119: DNS Server TCP Start successful  0.0.0.0:53



```

### 1.3 编译体验

> SampleDNS2 使用Redis作为数据库，请提前构建Redis数据库

### 1.3.1 clone 代码

```bash
# mkdir $GOPATH/src/gitee.com/pdudo 
# cd $GOPATH/src/gitee.com/pdudo 
# git clone https://gitee.com/pdudo/SampleDNSTool
# git clone https://gitee.com/pdudo/SampleDNS2
```

### 1.3.2 编译代码

```bash
# cd $GOPATH/src/gitee.com/pdudo/SampleDNS2
# go build
```

参照 1.2 二进制包

### 1.4 Web 维护域名信息

显示记录

支持【解析启动/停用 解析修改 和 删除】

![image-20211029155451750](imgs/index_2022_03_08.png)

添加记录

![image-20211029155544902](imgs/add_2022_03_08.png)


修改记录

![image-20211029155600411](imgs/update_2022_03_08.png)

删除记录

![image-20211029155614900](imgs/del_2022_03_08.png)


### 1.5 测试

使用ping/nslookup/dig即可测试

![image-20211029155829219](imgs/show_2022_03_08.png)

cname记录
页面展示
![show_2022-03-12_23-03-45.png](imgs/show_2022-03-12_23-03-45.png)

查询
![dig_2022-03-12_23-04-53.png](imgs/dig_2022-03-12_23-04-53.png)